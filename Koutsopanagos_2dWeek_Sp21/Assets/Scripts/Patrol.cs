﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public float speed;
    public Transform groundDectect;
    public GameObject player;
    public CanvasGroup gameEnd;
    public CanvasGroup ui;

    private bool movingRight = true;
    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        RaycastHit2D ground = Physics2D.Raycast(groundDectect.position, Vector2.down, 2f);
        if (ground.collider == false)
        {
            if(movingRight)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingRight = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingRight = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == player)
        {
            ui.alpha = 0;
            gameEnd.alpha = 1;
        }
    }
}
